const express = require('express');


// Initialize App
const app = express();
const PORT = 8081;


// Assign route
app.get("/", (request, response) =>
{
  response.send(`<h1>Main Index</h1>`);
});
app.get("/greeting", (request, response) =>
{
  response.send(`<h1>Hello, World!</h1>`);
});
app.get("/greeting/:name", (request, response) =>
{
  const name = request.params.name;
  response.send(`<h1>Hello, ${name}!</h1>`);
});
app.get("/tip/:total/:tipPercentage", (request, response) =>
{
  const tip = request.params.total * (request.params.tipPercentage / 100);
  response.send(`<h1>Tip Amound: $${String(tip).padStart(2, '0')}</h1>`);
});
app.get("/magic/:question", (request, response) =>
{
  const magic8Answers = [
    "It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes definitely",
    "You may rely on it",
    "As I see it yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",
    "Reply hazy try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful"
  ];

  const getMagic8Answer = function(magic8Answers)
  {
    const index = Math.floor(Math.random() * magic8Answers.length);
    return magic8Answers[index];
  };


  const html = `
    <h1>Magic8 Ball</h1>
    <h1>Question: ${request.params.question}</h1>
    <h1>Answer: ${getMagic8Answer(magic8Answers)}</h1>
  `;

  response.send(html);
});


// Start server on PORT.
app.listen(PORT, () =>
{
  console.log('Server started!' + PORT);
});

